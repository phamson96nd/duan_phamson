# Mục Đích
Tạo một tập dữ liệu chuẩn cho bài toán nhận dạng và liên kết thực thể.

# Các Kiểu Thực Thể
1. PER: thực thể chỉ người, ví dụ: Phạm Bảo Sơn
2. ORG: thực thể chỉ tổ chức, ví dụ: đội tuyển Việt Nam
3. LOC: thực thể chỉ địa điểm, ví dụ: Việt Nam
4. MISC: các loại thực thể có tên khác, ví dụ: giải vô địch quốc gia V-League

# Hướng Dẫn
1. Chỉ gán nhãn thực thể dài nhất. Các thực thể nằm trong một thực thể khác không được gán nhãn. 
Ví dụ 1: chỉ gán "đội tuyển Việt Nam" không gán cụm "Việt Nam" trong trường hợp này.
Ví dụ 2: chỉ gán "huyện Mỹ Đức" không gán cụm "Mỹ Đức".
2. Lưu ý chọn loại thực thể cho trong những trường hợp nhân hóa hoặc ngầm ám chỉ. Ví dụ: trong câu "Việt Nam giành ngôi á quân giải U23 châu Á.", thực thể "Việt Nam" chỉ tổ chức (đội bóng) không phải chỉ địa điểm nên phải được gán là ORG.

# Cách Gán Nhãn

## Văn bản chưa gán nhãn
Ninh Bình: Cụm công nghiệp vi phạm luật
(Dân trí) - Chưa có Báo cáo đánh giá tác động môi trường, tuy nhiên Cụm công nghiệp Cầu Yên (Ninh Bình) vẫn ngang nhiên san lấp mặt bằng, triển khai xây dựng nhiều hạng mục. Bộ TNMT đã chỉ rõ sai phạm, yêu cầu dừng hoạt động, khắc phục nhưng đến nay chủ đầu tư dự án vẫn… làm ngơ.

## Văn bản đã gán nhãn
[[Ninh Bình|LOC]]: Cụm công nghiệp vi phạm luật
([[Dân trí|ORG]]) - Chưa có Báo cáo đánh giá tác động môi trường, tuy nhiên [[Cụm công nghiệp Cầu Yên|LOC]] ([[Ninh Bình|LOC]]) vẫn ngang nhiên san lấp mặt bằng, triển khai xây dựng nhiều hạng mục. [[Bộ TNMT|ORG]] đã chỉ rõ sai phạm, yêu cầu dừng hoạt động, khắc phục nhưng đến nay chủ đầu tư dự án vẫn… làm ngơ.

# Tài Khoản
Link: https://dbncoder@bitbucket.org/datnguyenmpi/nerd-data.git
Passwords: dbnCoder
 
