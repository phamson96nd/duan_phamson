package preparation;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/***
 * 
 * @author datnb
 *
 */
public class PartitionData {
	public static void main(String args[]) throws Exception {
		Map<Integer, String> id2url = new HashMap<Integer, String>();
		Map<Integer, String> id2content = new HashMap<Integer, String>();
		String sourceFile = "./data/news";
		String sourceFolder = "./data/students";
		int THRESHOLD = 2000;
		int NUM_DOCS = 400;
		/**
		 * load data
		 */
		boolean nextArticle = true;
		int id = -1;
		String url = "";
		String content = "";
		for(String line: FileUtils.getAllLines(sourceFile)) {
			if(nextArticle) {
				nextArticle = false;
				String[] str = line.split(",");
				id = Integer.parseInt(str[0]);
				url = str[1];
				if(url.indexOf("tuoitre.vn") == -1)
					id2url.put(id, url);
				content += str[2];
			}
			else 
				content += "\n" + line;
			if(line.equalsIgnoreCase("\"") || line.indexOf("html?") != -1) {
				content = content.substring(1, content.length()-1);
				if(url.indexOf("tuoitre.vn") == -1 && content.length() >= THRESHOLD)
					id2content.put(id, content);
				if(id2content.size() >= NUM_DOCS)
					break;
				nextArticle = true;
				content = "";
			}
		}
		int[] ids = new int[id2content.size()];
		int pos = 0;
		for(int key: id2content.keySet()) {
			ids[pos++] = key;
		}
		int NUM_STUDENTS = 5;
		int start = 0;
		for(int i = 0; i < NUM_STUDENTS; i ++) {
			PrintWriter writer = new PrintWriter(sourceFolder + "/" + (i+1) + ".txt", "UTF-8");
			for(int j = 0; j < 100; j ++)
				// print doc # start + j
				writer.write("[PAGE-START]" + "\t" + ids[start + j]
						+ "\n" + id2content.get(ids[start + j]) + "\n");
			start += 75;
			writer.close();
		}
	}
}
