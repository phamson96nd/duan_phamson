package preparation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/***
 * Anything which helps to work on files goes here...
 * 
 * @author datnb
 *
 */
public class FileUtils {
	/***
	 * read all lines in a file, including empty ones.
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static List<String> getAllLines(String fileName) throws IOException {
		InputStream is = new FileInputStream(new File(fileName));
        List<String> content = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        String line;
        while ((line = reader.readLine()) != null) {
            content.add(line);
        }
        reader.close();
        return content;
    }
}
